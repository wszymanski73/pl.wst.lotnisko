<%@ page import="pl.wst.lotnisko.dao.KlientDaoHibernateImpl" %>
<%@ page import="java.util.List" %>
<%@ page import="pl.wst.lotnisko.domain.Klient" %><%--
  Created by IntelliJ IDEA.
  User: xxx
  Date: 11.08.2019
  Time: 09:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="pl">
<head>

    <meta charset="UTF-8" />
    <meta http-equiv=”U-XA-Compatible” content=”IE=edge, chrome=1” />
    <title>REZERWACJA</title>

    <link rel="stylesheet" href="resources/CSS/style.css" type="text/css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

</head>
<body>
<div id="conteiner">

    <%@ include file="logo.jsp" %>

    <%@ include file="menu.jsp" %>

    <article>
        <div id="content">
            <form id="rezerwacja" action="http://localhost:8080/lotnisko/addServlet" method="post">
                Klient:
                <% KlientDaoHibernateImpl klientDaoHibernate = new KlientDaoHibernateImpl();
                    List<Klient> klients = klientDaoHibernate.findAll();
                    String allUsers = "";
                    for (Klient a:klients) {
                        allUsers=allUsers+a;
                    }
                %>
                Data wylotu:
                <input type="date" name="dataod"></br>
                Data powrotu:
                <input type="date" name="datado"></br>
                <input type="submit" value="send"></br>
                <a href="index.jsp">POWRÓT</a>
            </form>
        </div>
    </article>

    <%@ include file="footer.jsp" %>

</div>

</body>
</html>
