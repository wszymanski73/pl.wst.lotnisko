<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<div id="logo">
    <h1>AIRPORT</h1>
</div>

<div id="image">
    <a><img src="resources/JPG/plane.jpg" width="80" height="60"/></a>
</div>

<div id="lang">
    <a href="/lotnisko/languageServlet?lang=en"><img src="resources/JPG/en_flag.jpg" width="40" height="20"/></a>
    <a href="/lotnisko/languageServlet?lang=pl"><img src="resources/JPG/pl_flag.jpg" width="40" height="20"/></a>

</div>
