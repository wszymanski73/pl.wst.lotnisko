<%@ page import="pl.wst.lotnisko.controler.LanguageProvider" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<nav>
    <div id="menu">
        <ul>
            <li><a href="index.jsp"><%= LanguageProvider.INSTANCE.getMessage("menu.index")%></a></li>
            <li><a href="firma.jsp"><%= LanguageProvider.INSTANCE.getMessage("menu.company")%></a></li>
            <li><a href="rezerwacja.jsp"><%= LanguageProvider.INSTANCE.getMessage("menu.reservation")%></a></li>
            <li><a href="klient.jsp"><%= LanguageProvider.INSTANCE.getMessage("menu.client")%></a></li>
            <li><a href="logowanie.jsp"><%= LanguageProvider.INSTANCE.getMessage("menu.logging")%></a></li>
            <li><a href="kontakt.jsp"><%= LanguageProvider.INSTANCE.getMessage("menu.contact")%></a></li>
        </ul>
    </div>
</nav>