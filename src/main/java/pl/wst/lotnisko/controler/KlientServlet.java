package pl.wst.lotnisko.controler;


import pl.wst.lotnisko.dao.KlientDaoHibernateImpl;
import pl.wst.lotnisko.domain.Klient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "klientServlet", urlPatterns = {"/klientServlet"})
public class KlientServlet extends HttpServlet {

    private static final String IMIE = "imie";
    private static final String NAZWISKO = "nazwisko";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String imie = req.getParameter(IMIE);
        String nazwisko = req.getParameter(NAZWISKO);

        Klient klient = new Klient(null,imie,nazwisko);
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        KlientDaoHibernateImpl klientDaoHibernate = new KlientDaoHibernateImpl();
        klientDaoHibernate.create(klient);

    }
}
