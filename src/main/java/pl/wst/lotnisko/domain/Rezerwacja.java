package pl.wst.lotnisko.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="rezerwacje")
public class Rezerwacja implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID",unique = true)
    private Integer id;

    @Column(name="data_od",nullable = false)
    private String dataod;

    @Column(name="data_do",nullable = false)
    private String datado;

//    @Embedded
//    @AttributeOverrides( {
//            @AttributeOverride(name = "imie", column = @Column(name = "imie",nullable = false)),
//            @AttributeOverride(name = "nazwisko", column = @Column(name = "nazwisko"))
//    })
//    private Klient klient;

}
