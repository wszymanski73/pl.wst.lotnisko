package pl.wst.lotnisko.dao;

import pl.wst.lotnisko.domain.Rezerwacja;

import java.util.List;
import java.util.Optional;

public interface RezerwacjaDao {
    List<Rezerwacja> findAll();

    Optional<Rezerwacja> findById(int id);

    int create(Rezerwacja rezerwacja);

    //int update(Person person);

    //int createOrUpdate(Person person);

    //void delete(int id);

    //void deleteAll();

    //Optional<Person> queryById(Integer id);

}
