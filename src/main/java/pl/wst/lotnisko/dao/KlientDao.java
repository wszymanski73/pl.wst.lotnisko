package pl.wst.lotnisko.dao;

import pl.wst.lotnisko.domain.Klient;

import java.util.List;
import java.util.Optional;

public interface KlientDao {
    List<Klient> findAll();

    Optional<Klient> findById(int id);

    int create(Klient klient);

    //int update(Person person);

    //int createOrUpdate(Person person);

    //void delete(int id);

    //void deleteAll();

    Optional<Klient> queryById(Integer id);

}
