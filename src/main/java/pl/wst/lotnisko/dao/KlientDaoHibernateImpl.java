package pl.wst.lotnisko.dao;

import lombok.NoArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import pl.wst.lotnisko.domain.Klient;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor
public class KlientDaoHibernateImpl implements KlientDao {

    private static final SessionFactory sessionFactory;

    static {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml")
                .build();
        Metadata metadata = new MetadataSources(registry)
                .buildMetadata();

        sessionFactory = metadata.buildSessionFactory();
    }

    @Override
    public List<Klient> findAll() {
        Session session = sessionFactory.openSession();
        Query<Klient> query = session.createQuery("FROM klienci",Klient.class);
        List<Klient> wynik = query.list();
        session.close();
        return wynik;
    }

    @Override
    public Optional<Klient> findById(int id) {
        Session session = sessionFactory.openSession();
        Klient klient = session.get(Klient.class, id);
        session.close();
        return Optional.ofNullable(klient);
    }

    @Override
    public int create(Klient klient) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.save(klient);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction.isActive()){
                transaction.rollback();
            }
            throw e;
        }
        session.close();
        return klient.getId();
    }

    @Override
    public Optional<Klient> queryById(Integer id) {

        Session session = sessionFactory.openSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Klient> criteria = builder.createQuery(Klient.class);
        Root<Klient> root = criteria.from(Klient.class);

        Predicate idPredicate = builder.equal(root.get("id"),id);

        criteria.select(root).where(idPredicate);
        Query<Klient> query = session.createQuery(criteria);
        Klient klient = query.getSingleResult();
        session.close();

        return Optional.ofNullable(klient);
    }
}
