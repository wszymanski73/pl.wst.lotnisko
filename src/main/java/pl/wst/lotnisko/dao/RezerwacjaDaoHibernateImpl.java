package pl.wst.lotnisko.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import pl.wst.lotnisko.domain.Rezerwacja;

import java.util.List;
import java.util.Optional;

public class RezerwacjaDaoHibernateImpl implements RezerwacjaDao {

    private static final SessionFactory sessionFactory;

    static {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml")
                .build();
        Metadata metadata = new MetadataSources(registry)
                .buildMetadata();

        sessionFactory = metadata.buildSessionFactory();
    }

    public RezerwacjaDaoHibernateImpl() {
    }

    @Override
    public List<Rezerwacja> findAll() {
        Session session = sessionFactory.openSession();
        Query<Rezerwacja> query = session.createQuery("FROM rezerwacje",Rezerwacja.class);
        List<Rezerwacja> wynik = query.list();
        session.close();
        return wynik;
    }

    @Override
    public Optional<Rezerwacja> findById(int id) {
        Session session = sessionFactory.openSession();
        Rezerwacja rezerwacja = session.get(Rezerwacja.class, id);
        session.close();
        return Optional.ofNullable(rezerwacja);
    }

    @Override
    public int create(Rezerwacja rezerwacja) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.save(rezerwacja);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction.isActive()){
                transaction.rollback();
            }
            throw e;
        }
        session.close();
        return rezerwacja.getId();
    }
}
